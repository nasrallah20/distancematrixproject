async function continueMatrix() {

    //On récupère les éléments de la matrice de la base de données
    const response = await fetch('/api');
    const data = await response.json();
    
    let distanceMatrix = [];
    let timeMatrix = [];

    //On récupère les adresses de la base de données
    const addressResponse = await fetch('/address');
    const addressData = await addressResponse.json();

    const output = [];
    const longLatArray = [];
    let longLat = "";
    const container = document.getElementById("output");
    let counterAPI = 0;
    let inserted = false;

    //Si data n'est pas vide, on continue la matrice
    if(data.length !== 0){

        //On récupère la taille de la matrice
        const nbOfMatrixElements = data[0].nbOfMatrixElements;

        //On calcule le nombre d'éléments de la matrice
        const matrixElements = (nbOfMatrixElements * nbOfMatrixElements) - nbOfMatrixElements;

        //On calcule le temps restant pour calculer la matrice
        const TIME_LIMIT = (matrixElements - data.length) * 2;
        let timePassed = 0;
        let timeLeft = TIME_LIMIT;

        //On récupère le nombre de données existants dans la base de données
        counterAPI = data.length;

        //On affiche le timer dans la page HTML
        document.getElementById("timer").innerHTML = `
        <div class="base-timer">
          <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
            <g class="base-timer__circle">
              <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45" />
            </g>
          </svg>
          <span id="base-timer-label" class="base-timer__label">
            ${formatTime(timeLeft)}
          </span>
        </div>
        `;

        //On décrémente le timer chaque seconde
        timerInterval = setInterval(() => {

            timePassed = timePassed += 1;
            timeLeft = TIME_LIMIT - timePassed;

            if(timeLeft === 0){
                clearInterval(timerInterval);
            }

            document.getElementById("base-timer-label").innerHTML = formatTime(timeLeft);
        }, 1000);

        //On récupère les adresses de la base de données pour les afficher dans la page HTML
        addressData.forEach((obj, i) => {
            output.push(obj.address);
            container.innerHTML += i+1 + " : " + obj.address + "</br>";

            longLat = obj.long + "," + obj.lat;
            longLatArray.push(longLat)
        })

        //On initialise les matrices
        for (let line = 0; line <= nbOfMatrixElements ; line++) {
            distanceMatrix[line] = new Array(nbOfMatrixElements + 1);
            timeMatrix[line] = new Array(nbOfMatrixElements + 1);
        }

        //On affiche des index dans la matrice
        for(let line = 0; line <= nbOfMatrixElements ; line++){
            for(let column = 0; column <= nbOfMatrixElements; column++){

                distanceMatrix[line][0] = line;
                distanceMatrix[0][column] = column + "&nbsp;&nbsp;";

                timeMatrix[line][0] = line;
                timeMatrix[0][column] = column + "&nbsp;&nbsp;";
            }
        }

        //On insère les données dans la matrice
        for(let line = 1; line <= nbOfMatrixElements; line++) {
            for (let column = 1; column <= nbOfMatrixElements; column++) {

                //On met des 0 dans la daigonale
                if( line === column) {
                    distanceMatrix[line][column] = 0;
                    timeMatrix[line][column] = 0;
                }
                else {

                    data.reverse().forEach(obj => {
                        //Si on retrouve des objets dans la base de données, on les mets directement
                        //dans la base de données
                        if (obj.line !== obj.column && obj.inserted === true) {
                            distanceMatrix[obj.line][obj.column] = ((obj.distance) / 1000).toFixed(2);
                            timeMatrix[obj.line][obj.column] = ((obj.time) / 60).toFixed(2);
                            
                        }
                    })

                    //Si on retrouve pas les données dans la base de données, on calcule la distance avec l'API
                    if(typeof(distanceMatrix[line][column]) === 'undefined') {
                        fetch(reformatUrlDistance(longLatArray[line - 1], longLatArray[column - 1]))
                            .then(res => res.json())
                            .then(resJson => {
                                distance = resJson.routes[0].distance;
                                time = resJson.routes[0].duration;
                                distanceMatrix[line][column] = (distance / 1000).toFixed(2);
                                timeMatrix[line][column] = (time / 60).toFixed(2);

                                counterAPI++;

                                inserted = true;

                                //On continue l'insertion dans la base de données
                                const data = {
                                    line,
                                    column,
                                    distance,
                                    time,
                                    counterAPI,
                                    nbOfMatrixElements,
                                    inserted
                                };
    
                                const options = {
                                    method : 'POST',
                                    headers : {
                                        'Content-Type': 'application/json'
                                    },
                                    body : JSON.stringify(data)
                                }
                                fetch('/api', options);
                            });
                        //Délais de deux scondes entre les requêtes
                        await sleep(2000);
                    }
                }
            }
        }

        const distMatrix = document.getElementById("distanceMatrix");
        const durationMatrix = document.getElementById("timeMatrix");
        let labelDistanceMatrix = document.getElementById("labelDistanceMatrix");
        let labelTimeMatrix = document.getElementById("labelTimeMatrix");
        labelDistanceMatrix.innerHTML = "Distance Matrix (Km)";
        labelTimeMatrix.innerHTML = "Time Matrix (Min)";


        //On affiche la matrice dans la page HTML
        for(let line = 0; line < nbOfMatrixElements + 1; line++){
            for(let column = 0; column < nbOfMatrixElements + 1 ; column++){
                distMatrix.innerHTML += distanceMatrix[line][column] + "&emsp;&emsp;&nbsp;";
                durationMatrix.innerHTML += timeMatrix[line][column] + "&emsp;&emsp;&nbsp;";
            }
            distMatrix.innerHTML += "<br>";
            durationMatrix.innerHTML += "<br>";
        }

    }
    //Si on retrouve pas de données dans la base de données
    else {
        alert("You didn't start any matrix")
    }

}

//On transforme la requête d'OSRM 
function reformatUrlDistance(origin, destination){

    return "http://router.project-osrm.org/route/v1/driving/" +
        origin + ";" + destination + "?overview=false"
}

//Formattage du temps en minutes et secondes
function formatTime(time) {
    const minutes = Math.floor(time / 60);

    let seconds = time % 60;

    if (seconds < 10) {
        seconds = `0${seconds}`;
    }

    //L'affichage en format MM:SS 
    return `${minutes}:${seconds}`;
}