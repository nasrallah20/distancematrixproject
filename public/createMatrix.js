//Fonction qui lit le fichier d'entrée
const readUploadedFileAsText = (inputFile) => {

    const temporaryFileReader = new FileReader();

    return new Promise((resolve, reject) => {
        temporaryFileReader.onerror = () => {
            temporaryFileReader.abort();
            reject(new DOMException("Problem parsing input file."));
        };

        temporaryFileReader.onload = () => {
            resolve(temporaryFileReader.result);
        };
        temporaryFileReader.readAsText(inputFile);
    });
};

//Fonction qui fait un délais de temps de 2 secondes
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//On commence à gérer la contruction de la matrice
const handleUpload = async (event) => {

    //On récupère le fichier d'entrée
    const file = event.target.files[0];

    try {
        //On le lit
        const fileContents = await readUploadedFileAsText(file);

        //On split les caque ligne
        const lines = fileContents.split('\n');

        const allLongLat = [];

        let address = "";

        let inserted = false;

        let counterAPI = 0;

        //La taille de la matrice
        const nbOfMatrixElements = lines.length;

        let labelAddress = document.getElementById("labelAddress");
        labelAddress.innerHTML = "List of Addresses";


        //On affiche du tableau des indices
        const container = document.getElementById("output");

        //Temps limite pour finir la matrice (On enlève la diagonale et on multiplie par 2,
        // le délais entre deux requêtes)
        const TIME_LIMIT = ((lines.length * lines.length) - lines.length) * 2;

        // Initially, no time has passed, but this will count up
        // and subtract from the TIME_LIMIT
        let timePassed = 0;
        let timeLeft = TIME_LIMIT;

        //On rajoute le timer dans la page HTML
        document.getElementById("timer").innerHTML = `
        <div class="base-timer">
          <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
            <g class="base-timer__circle">
              <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45" />
            </g>
          </svg>
          <span id="base-timer-label" class="base-timer__label">
            ${formatTime(timeLeft)}
          </span>
        </div>
        `;

        //On fait un intervalle d'une seconde pour décrémenter le timer
        timerInterval = setInterval(() => {

            //On incrémente le temps passé
            timePassed = timePassed += 1;
            timeLeft = TIME_LIMIT - timePassed;

            //Lorqu'on atteint 0, on arrête l'intervalle
            if(timeLeft === 0){
                clearInterval(timerInterval);
            }

            //On met à jour le label de temps
            document.getElementById("base-timer-label").innerHTML = formatTime(timeLeft);
        }, 1000);

        //Pour chaque ligne récupérée du fichier, on récupère la longitude et la latitude
        lines.forEach((longLat, i) => {

            let long = longLat.split(",")[0];
            let lat = longLat.split(",")[1];

            //On rajoute les lignes dans le tableau 
            allLongLat.push(longLat);

            //On envoie des requêtes au serveur pour enregistrer les adresses dans la base de données
            setTimeout(() => { fetch(reformatUrlGeocoding(lat, long))
                .then(res => res.json())
                .then(resJson => {
                    address = resJson.address.road + " " +resJson.address.postcode;
                    //On ajoute les adresses à la page HTML
                    container.innerHTML += i+1 + " : " + address + "</br>";

                    //On crée un objet adresse pour le rajouter dans la base de données
                    const addressData = {
                        address,
                        long,
                        lat,
                        nbOfMatrixElements
                    };

                    const options = {
                        method : 'POST',
                        headers : {
                            'Content-Type': 'application/json'
                        },
                        body : JSON.stringify(addressData)
                    }
                    fetch('/address', options);
                })
            } , i * 1000);
        });

        let distanceMatrix = [];
        let timeMatrix = [];
        let distance;

        //On initialise les deux matrices de temps ou de distances
        for (let line = 0; line <= allLongLat.length + 1 ; line++) {
            distanceMatrix[line] = new Array(allLongLat.length + 1);
            timeMatrix[line] = new Array(allLongLat.length + 1);
        }

        //On ajoute une ligne et une colonne pour les index pour les matrices
        for(let line = 0; line <= allLongLat.length; line++){
            for(let column = 0; column <= allLongLat.length; column++){

                distanceMatrix[line][0] = line;
                distanceMatrix[0][column] = column + "&emsp;";

                timeMatrix[line][0] = line;
                timeMatrix[0][column] = column + "&emsp;";
            }
        }

        //Boucle de remplissage de la matrice
        for(let line = 1; line <= allLongLat.length; line++) {
            for (let column = 1; column <= allLongLat.length; column++) {

                //Si on se retrouve sur la diagonale on met des 0
                if(line === column) {
                    distanceMatrix[line][column] = 0;
                    timeMatrix[line][column] = 0;
                }
                else {

                    //Utilisation de l'API OSRM pour récupérer la distance
                    fetch(reformatUrlDistance(allLongLat[line - 1], allLongLat[column - 1]))
                        .then(res => res.json())
                        .then(resJson => {
                            //On récupère la distance et la durée de l'objet JSON de la requête
                            distance = resJson.routes[0].distance;
                            time = resJson.routes[0].duration;
                            distanceMatrix[line][column] = (distance/1000).toFixed(2);
                            timeMatrix[line][column] = (time/60).toFixed(2);

            
                            counterAPI ++;
                            inserted = true;

                            //On crée un objet JSON et on l'envoie à la base de données
                            const data = {
                                line,
                                column,
                                distance,
                                time,
                                counterAPI,
                                nbOfMatrixElements,
                                inserted
                            };

                            const options = {
                                method : 'POST',
                                headers : {
                                    'Content-Type': 'application/json'
                                },
                                body : JSON.stringify(data)
                            }
                            fetch('/api', options);
                        })
                    //Délais de 2 secondes entre deux requêtes
                    await sleep(2000);
                }
            }
        }

        //On affiche la matrice
        const distMatrix = document.getElementById("distanceMatrix");
        const duraMatrix = document.getElementById("timeMatrix");

        let labelDistanceMatrix = document.getElementById("labelDistanceMatrix");
        let labelTimeMatrix = document.getElementById("labelTimeMatrix");

        labelDistanceMatrix.innerHTML = "Distance Matrix (Km)";
        labelTimeMatrix.innerHTML = "Time Matrix (Min)";

        for(let line = 0; line < allLongLat.length + 1; line++){
            for(let column = 0; column < allLongLat.length + 1 ; column++){

                distMatrix.innerHTML += distanceMatrix[line][column] + "&emsp;&emsp;&nbsp;";
                duraMatrix.innerHTML += timeMatrix[line][column] + "&emsp;&emsp;&nbsp;";

            }
            distMatrix.innerHTML += "<br>";
            duraMatrix.innerHTML += "<br>";
        }


    } catch (e) {
        console.log('error :', e)
    }
}

document.querySelector('input#fileInput').addEventListener('change', handleUpload)


//On utilise la longitude et latitude pour les mettre dans l'URL
function reformatUrlGeocoding(lat, lon){

    //Reformatted string to http nominatim request
    return "https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat="
        + lat
        + "&lon="
        + lon
}

//On transforme la requête d'OSRM 
function reformatUrlDistance(origin, destination){

    return "http://router.project-osrm.org/route/v1/driving/" +
        origin + ";" + destination + "?overview=false"
}

//Formattage du temps en minutes et secondes
function formatTime(time) {
    const minutes = Math.floor(time / 60);

    let seconds = time % 60;

    if (seconds < 10) {
        seconds = `0${seconds}`;
    }

    //L'affichage en format MM:SS 
    return `${minutes}:${seconds}`;
}
