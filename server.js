const express = require('express');
const DataStore = require('nedb');

const app = express();

//Utilisation du port 3000
app.listen(3000, () => console.log('listening at 3000'));
app.use(express.static("public"));
app.use(express.json());

//Base de données qui enregistre les données de la matrice
const database = new DataStore('database.db');
database.loadDatabase();

//Base de données qui enregistre les adresses
const addressDb = new DataStore('addressDb.db');
addressDb.loadDatabase();

//Requête POST pour ajouter dans la base de données
app.post('/api', ((request, response) => {

    const data = request.body;
    //On ajoute l'élément de la requête dans la base de données
    database.insert(data);
    response.end();
    //Lorsqu'on la matrice est finie, on supprime les données de la base de données
    if(request.body.counterAPI === ((request.body.nbOfMatrixElements * request.body.nbOfMatrixElements) - request.body.nbOfMatrixElements )){
        database.remove({}, { multi: true }, function (err, numRemoved) {
            database.persistence.compactDatafile()
        });
    }
}));

//Requête POST pour ajouter les données de la base de données
app.post('/address', ((request, response) => {
    //On ajoute les adresses dans la base de données
    const data = request.body;
    addressDb.insert(data);
    response.end();
}));

//Requête GET pour récupérer les éléments de la matrice de la base de données
app.get('/api',(request, response) => {
    database.find({}, (err, data) => {
        if(err){
            response.end();
            return;
        }
        //On récupère les données sous forme de JSON
        response.json(data);
    });
});

//Requête GET pour récupérer les adresses de la base de données
app.get('/address',(request, response) => {
    addressDb.find({}, (err, data) => {
        if(err){
            response.end();
            return;
        }       
         //On récupère les données sous forme de JSON
        response.json(data);
    });
});
